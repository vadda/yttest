package com.yt.test;

import com.yt.test.model.TreatmentAction;
import com.yt.test.model.TreatmentPlan;
import com.yt.test.model.TreatmentTask;
import com.yt.test.repository.TreatmentPlanRepository;
import com.yt.test.repository.TreatmentTaskRepository;
import com.yt.test.scheduler.TreatmentPlanScheduler;
import com.yt.test.service.TreatmentService;
import lombok.extern.java.Log;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.support.CronExpression;
import org.springframework.test.context.ActiveProfiles;

import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;
import java.util.function.Function;

import static com.yt.test.service.Constants.DATETIME_FORMATTER;
import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles("test")
@SpringBootTest
@Log
public class TreatmentPlanSchedulerTests {

    @Autowired
    private TreatmentService treatmentService;

    @Autowired
    private TreatmentPlanRepository treatmentPlanRepository;

    @Autowired
    private TreatmentTaskRepository treatmentTaskRepository;

    private TreatmentPlanScheduler scheduler;

    @BeforeEach
    public void init() {
        treatmentTaskRepository.deleteAll();
        treatmentPlanRepository.deleteAll();
        scheduler = new TreatmentPlanScheduler(treatmentService);
    }

    @Test
    public void testTwoPlansWithOverlappingSchedules() throws InterruptedException {
        log.info("test can take up to 2 minutes");
        log.info(() -> "test started at %s".formatted(DATETIME_FORMATTER.format(OffsetDateTime.now())));
        final OffsetDateTime now = waitForBeginningOfMinute();
        final var seconds = 60;

        // schedule every 11 seconds
        // starting for 00 second there should be 6 tasks created
        // at seconds: 11, 22, 33, 44, 55, 60
        final TreatmentPlan plan1 = treatmentService.makeTreatmentPlan(
                "John Smith",
                now,
                now.plusSeconds(seconds),
                TreatmentAction.ACTION_A,
                "0/11 * * ? * *");

        // schedule every 22 seconds
        // starting for 00 second there should be 3 tasks created
        // at seconds: 22, 44, 60
        final TreatmentPlan plan2 = treatmentService.makeTreatmentPlan(
                "James Brown",
                now,
                now.plusSeconds(seconds),
                TreatmentAction.ACTION_A,
                "0/22 * * ? * *");
        log.info("starting scheduler");
        scheduler.startScheduler();

        log.info("sleep for %d seconds".formatted(seconds));
        Thread.sleep(seconds * 1000);


        final List<TreatmentTask> tasks = treatmentService.getAllActiveTasks();
        assertThat(tasks)
                .hasSize(9)
                .extracting((Function<TreatmentTask, OffsetDateTime>) TreatmentTask::getTimeCreated)
                .contains(
                        // plan 1
                        now.plusSeconds(11),
                        now.plusSeconds(2 * 11),
                        now.plusSeconds(3 * 11),
                        now.plusSeconds(4 * 11),
                        now.plusSeconds(5 * 11),
                        // plan 2
                        now.plusSeconds(22),
                        now.plusSeconds(2 * 22),
                        // plan 1 & 2
                        now.plusSeconds(60)
                );

        final List<TreatmentPlan> plans = Arrays.asList(plan1, plan2);
        plans.forEach(plan ->
                assertThat(tasks.stream().filter(t -> t.getTreatmentPlanId().equals(plan.getId())))
                        .hasSize(plan == plan1 ? 6 : 3)
                        .allSatisfy((Consumer<TreatmentTask>) task -> {
                            assertThat(task.getAction()).isEqualTo(plan.getAction());
                            assertThat(task.getPatient()).isEqualTo(plan.getPatient());
                            assertThat(task.getStatus()).isEqualTo(plan.getStatus());
                        }));
        scheduler.shutdown();
    }

    @Test
    public void testSinglePlan() throws InterruptedException {
        log.info(() -> "test started at %s".formatted(DATETIME_FORMATTER.format(OffsetDateTime.now())));
        log.info("test can take up to 2 minutes");
        final OffsetDateTime now = waitForBeginningOfMinute();
        final var seconds = 60;

        // schedule every 11 seconds
        // starting for 00 second there should be 6 tasks created
        // at seconds: 11, 22, 33, 44, 55, 60

        final TreatmentPlan plan = treatmentService.makeTreatmentPlan(
                "John Brown",
                now,
                now.plusSeconds(seconds),
                TreatmentAction.ACTION_A,
                "0/11 * * ? * *");
        log.info("starting scheduler");
        scheduler.startScheduler();

        log.info("sleep for %d seconds".formatted(seconds));
        Thread.sleep(seconds * 1000);


        final List<TreatmentTask> tasks = treatmentService.getAllActiveTasks();
        assertThat(tasks)
                .hasSize(6)
                .extracting((Function<TreatmentTask, OffsetDateTime>) TreatmentTask::getTimeCreated)
                .contains(
                        now.plusSeconds(11),
                        now.plusSeconds(2 * 11),
                        now.plusSeconds(3 * 11),
                        now.plusSeconds(4 * 11),
                        now.plusSeconds(5 * 11),
                        now.plusSeconds(60)
                );
        assertThat(tasks.stream().allMatch(t ->
                t.getAction().equals(plan.getAction()) &&
                        t.getPatient().equals(plan.getPatient()) &&
                        t.getStatus().equals(plan.getStatus()) &&
                        t.getTreatmentPlanId().equals(plan.getId())
        )).isTrue();
        scheduler.shutdown();
    }

    private OffsetDateTime waitForBeginningOfMinute() {
        OffsetDateTime now = OffsetDateTime.now().truncatedTo(ChronoUnit.SECONDS);
        final OffsetDateTime next = CronExpression.parse("0 0/1 * 1/1 * ?").next(now);
        final long millis = ChronoUnit.MILLIS.between(now, next);

        log.info("waiting for beginning of a minute (ETA: %d ms)".formatted(millis));
        final ScheduledFuture<?> scheduled = Executors.newSingleThreadScheduledExecutor().schedule(
                () -> log.info("waited for beginning of a minute"),
                millis,
                TimeUnit.MILLISECONDS);

        try {
            scheduled.get(millis + 100, TimeUnit.MILLISECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException ignored) {
            // ignored
        }

        return next.truncatedTo(ChronoUnit.SECONDS);
    }
}
