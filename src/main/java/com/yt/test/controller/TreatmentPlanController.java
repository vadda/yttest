package com.yt.test.controller;

import com.yt.test.model.TreatmentPlan;
import com.yt.test.service.TreatmentService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/plans")
public class TreatmentPlanController {
    private final TreatmentService treatmentService;

    public TreatmentPlanController(TreatmentService treatmentService) {
        this.treatmentService = treatmentService;
    }

    @GetMapping("all")
    public ResponseEntity<List<TreatmentPlan>> getPlans() {
        return ResponseEntity.ok(treatmentService.getActivePlans());
    }

}
