package com.yt.test.controller;

import com.yt.test.model.TreatmentTask;
import com.yt.test.service.TreatmentService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/tasks")
public class TreatmentTaskController {
    private final TreatmentService treatmentService;

    public TreatmentTaskController(TreatmentService treatmentService) {
        this.treatmentService = treatmentService;
    }

    @GetMapping("all")
    public ResponseEntity<List<TreatmentTask>> getTasks() {
        return ResponseEntity.ok(treatmentService.getAllActiveTasks());
    }

}
