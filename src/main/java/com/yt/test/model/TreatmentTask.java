package com.yt.test.model;

import com.yt.test.service.Constants;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.UuidGenerator;

import java.time.OffsetDateTime;

@Entity
@Getter
@Setter
public class TreatmentTask {
    @Id
    @UuidGenerator
    private String id;
    private String treatmentPlanId;
    private TreatmentAction action;
    private String patient;
    private OffsetDateTime timeCreated;
    private OffsetDateTime timeDone;
    private String status = Constants.ActiveStatus;
}
