package com.yt.test.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.UuidGenerator;

import java.util.Objects;

@Entity
@Getter
@Setter
public class TreatmentPlan {
    @Id
    @UuidGenerator
    private String id;
    private String patient;
    private TreatmentAction action;
    private java.time.OffsetDateTime startTime;
    private java.time.OffsetDateTime endTime;
    private String recurrencePattern;
    private String status;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TreatmentPlan that = (TreatmentPlan) o;
        return Objects.equals(id, that.id) && Objects.equals(patient, that.patient) && action == that.action && Objects.equals(startTime, that.startTime) && Objects.equals(endTime, that.endTime) && Objects.equals(recurrencePattern, that.recurrencePattern) && Objects.equals(status, that.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, patient, action, startTime, endTime, recurrencePattern, status);
    }

}
