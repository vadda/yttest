package com.yt.test.model;


public enum TreatmentAction {
    ACTION_A("Action A"),
    ACTION_B("Action B"),
    ;
    final String name;

    TreatmentAction(String name) {
        this.name = name;
    }
}
