package com.yt.test.repository;

import com.yt.test.model.TreatmentTask;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TreatmentTaskRepository extends JpaRepository<TreatmentTask, String> {

    List<TreatmentTask> findByStatus(String status);
}
