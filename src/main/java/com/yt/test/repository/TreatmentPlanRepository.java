package com.yt.test.repository;

import com.yt.test.model.TreatmentPlan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TreatmentPlanRepository extends JpaRepository<TreatmentPlan, String> {
    List<TreatmentPlan> findByStatus(String status);
}
