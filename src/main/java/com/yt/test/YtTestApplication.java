package com.yt.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = "com.yt.test.repository")
public class YtTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(YtTestApplication.class, args);
	}

}
