package com.yt.test.service;

import com.yt.test.model.TreatmentAction;
import com.yt.test.model.TreatmentPlan;
import com.yt.test.model.TreatmentTask;
import com.yt.test.repository.TreatmentPlanRepository;
import com.yt.test.repository.TreatmentTaskRepository;
import lombok.extern.java.Log;
import org.springframework.scheduling.support.CronExpression;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.List;

import static com.yt.test.service.Constants.DATETIME_FORMATTER;

@Service
@Log
public class TreatmentService {

    private final TreatmentPlanRepository treatmentPlanRepository;
    private final TreatmentTaskRepository treatmentTaskRepository;

    public TreatmentService(TreatmentPlanRepository treatmentPlanRepository, TreatmentTaskRepository treatmentTaskRepository) {
        this.treatmentPlanRepository = treatmentPlanRepository;
        this.treatmentTaskRepository = treatmentTaskRepository;
    }

    public List<TreatmentTask> getAllActiveTasks() {
        return this.treatmentTaskRepository.findByStatus(Constants.ActiveStatus);
    }

    public List<TreatmentPlan> getActivePlans() {
        return this.treatmentPlanRepository.findByStatus(Constants.ActiveStatus);
    }

    public TreatmentTask makeTreatmentTask(TreatmentPlan treatmentPlan, OffsetDateTime timeCreated) {
        final TreatmentTask treatmentTask = new TreatmentTask();
        treatmentTask.setTreatmentPlanId(treatmentPlan.getId());
        treatmentTask.setAction(treatmentPlan.getAction());
        treatmentTask.setStatus(Constants.ActiveStatus);
        treatmentTask.setPatient(treatmentPlan.getPatient());
        treatmentTask.setTimeDone(null);
        treatmentTask.setTimeCreated(timeCreated);
        return this.treatmentTaskRepository.save(treatmentTask);
    }

    public TreatmentPlan makeTreatmentPlan(String patient,
                                           OffsetDateTime startDateTime,
                                           OffsetDateTime endDateTime,
                                           TreatmentAction treatmentAction,
                                           String recurrencePattern) {
        final TreatmentPlan treatmentPlan = new TreatmentPlan();
        treatmentPlan.setAction(treatmentAction);
        treatmentPlan.setPatient(patient);
        treatmentPlan.setStatus("Active");
        treatmentPlan.setStartTime(startDateTime);
        treatmentPlan.setEndTime(endDateTime);
        treatmentPlan.setRecurrencePattern(recurrencePattern);
        return treatmentPlanRepository.save(treatmentPlan);
    }

    public OffsetDateTime getNearestTaskStartTime(TreatmentPlan treatmentPlan, OffsetDateTime dateFrom) {
        if (!CronExpression.isValidExpression(treatmentPlan.getRecurrencePattern())) {
            log.severe("treatment plan (id=%s) has invalid recurrence pattern \"%s\"".formatted(treatmentPlan.getId(), treatmentPlan.getRecurrencePattern()));
            return null;
        }

        final CronExpression cronExpression = CronExpression.parse(treatmentPlan.getRecurrencePattern());
        final OffsetDateTime next = cronExpression.next(dateFrom);
        final OffsetDateTime nextTaskTime = next == null || next.isAfter(treatmentPlan.getEndTime()) ? null : next;

        log.info(() -> "pattern=%s,   dateSince=%s,   next date=%s".formatted(
                treatmentPlan.getRecurrencePattern(),
                DATETIME_FORMATTER.format(dateFrom),
                nextTaskTime == null ? "null" : DATETIME_FORMATTER.format(nextTaskTime)));

        return nextTaskTime;
    }
}
