package com.yt.test.service;

import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Locale;

public final class Constants {
    public static final String ActiveStatus = "Active";

    public static final String CONTEXT_PATH = "/treatment";

    public static final DateTimeFormatter DATETIME_FORMATTER = new DateTimeFormatterBuilder()
            .appendPattern("uuuu-MM-dd'T'HH:mm.ss")
            .toFormatter(Locale.ROOT);

    private Constants() {
        //
    }
}
