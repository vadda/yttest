package com.yt.test.scheduler;

import com.yt.test.model.TreatmentPlan;
import com.yt.test.service.TreatmentService;
import lombok.extern.java.Log;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.yt.test.service.Constants.DATETIME_FORMATTER;

@Service
@Log
public class TreatmentPlanScheduler {
    private final TreatmentService treatmentService;
    private final ConcurrentMap<TreatmentPlan, OffsetDateTime> plans = new ConcurrentHashMap<>();
    private final ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1);
    private final ExecutorService executorService = Executors.newCachedThreadPool();
    private final Object lock = new Object();
    private ScheduledFuture<?> schedulerFuture;

    public TreatmentPlanScheduler(TreatmentService treatmentService) {
        this.treatmentService = treatmentService;
    }

    public void startScheduler() {
        startScheduler(OffsetDateTime.now());
    }

    public void startScheduler(OffsetDateTime start) {
        final OffsetDateTime now = start == null ? OffsetDateTime.now() : start;

        synchronized (this.lock) {
            try {
                this.shutdown();
            } catch (InterruptedException ignored) {
                //
            }
            clearTaskDates();

            this.treatmentService.getActivePlans().forEach(plan -> {
                final OffsetDateTime nearestTaskStartTime = this.treatmentService.getNearestTaskStartTime(plan, now);
                if (nearestTaskStartTime == null) {
                    log.info(() -> "plan (id=%s) has no pending task".formatted(plan.getId()));
                } else {
                    this.plans.put(plan, nearestTaskStartTime);
                }
            });
        }
        this.schedulerFuture = this.scheduleNext(now);
    }

    public void shutdown() throws InterruptedException {
        shutdown(true, 3000);
    }

    public void shutdown(boolean force, long timeoutMillis) throws InterruptedException {
        synchronized (lock) {
            if (schedulerFuture != null && !schedulerFuture.isCancelled() && !schedulerFuture.isDone()) {
                schedulerFuture.cancel(force);
            }
            schedulerFuture = null;
            if (force) {
                scheduledExecutorService.shutdownNow();
                executorService.shutdownNow();
            } else {
                scheduledExecutorService.shutdown();
                executorService.shutdown();
            }
            scheduledExecutorService.awaitTermination(timeoutMillis, TimeUnit.MILLISECONDS);
            executorService.awaitTermination(timeoutMillis, TimeUnit.MILLISECONDS);
        }
    }

    private ScheduledFuture<?> scheduleNext(OffsetDateTime dateTimeSince) {
        log.info(() -> "scheduleNext: dateTimeSince=%s".formatted(DATETIME_FORMATTER.format(dateTimeSince)));

        final List<TreatmentPlan> nearestTasks = this.getNearestTasksSince(dateTimeSince);
        if (nearestTasks.isEmpty()) {
            log.info(() -> "didn't find any plans with pending tasks planned after %s".formatted(DATETIME_FORMATTER.format(dateTimeSince)));
            return null;
        }
        final OffsetDateTime nextDateTime = getNearestTaskDateTime(nearestTasks.get(0));
        if (nextDateTime == null) {
            log.info(() -> "schedule for treatment plan with (id=%s) doesn't have any pending tasks".formatted(
                    nearestTasks.stream().map(TreatmentPlan::getId).collect(Collectors.joining(","))));
            return null;
        } else {
            log.info(() -> "nextDateTime=%s".formatted(DATETIME_FORMATTER.format(nextDateTime)));
        }

        final long millis = ChronoUnit.MILLIS.between(dateTimeSince, nextDateTime);

        log.info(() -> "scheduling next task (%s) in %d ms".formatted(nearestTasks.stream().map(TreatmentPlan::getId).collect(Collectors.joining(",")), millis));
        return this.scheduledExecutorService.schedule(() -> this.makeTaskAndScheduleNext(nearestTasks, nextDateTime), millis, TimeUnit.MILLISECONDS);
    }

    private void makeTaskAndScheduleNext(List<TreatmentPlan> treatmentPlans, OffsetDateTime dateTime) {
        log.info(() -> "makeTaskAndScheduleNext: woke up at %s".formatted(DATETIME_FORMATTER.format(dateTime)));
        executorService.submit(() -> this.makeTasks(treatmentPlans, dateTime));
        final OffsetDateTime dateSince = OffsetDateTime.now();

        treatmentPlans.forEach(plan -> {
            final OffsetDateTime nearestTaskStartTime = this.treatmentService.getNearestTaskStartTime(plan, dateSince);
            log.info(() -> "plan (id=%s) next datetime %s".formatted(plan.getId(), DATETIME_FORMATTER.format(nearestTaskStartTime)));
            updateNextTaskDateTime(plan, nearestTaskStartTime);
        });

        this.schedulerFuture = this.scheduleNext(dateTime);
    }

    private void makeTasks(List<TreatmentPlan> treatmentPlans, OffsetDateTime dateTime) {
        treatmentPlans.forEach(plan -> this.treatmentService.makeTreatmentTask(plan, dateTime));
    }

    private List<TreatmentPlan> getNearestTasksSince(OffsetDateTime sinceDateTime) {
        log.info(() -> "getNearestTasksSince(%s)".formatted(DATETIME_FORMATTER.format(sinceDateTime)));
        log.info("plans: %s".formatted(getPlansString()));

        synchronized (this.lock) {
            final List<Map.Entry<TreatmentPlan, OffsetDateTime>> nearestEntries = this.plans.entrySet().stream()
                    .filter(plan -> plan.getValue() != null && plan.getValue().isAfter(sinceDateTime))
                    .sorted(Map.Entry.comparingByValue())
                    .toList();
            if (nearestEntries.isEmpty()) {
                return Collections.emptyList();
            }
            final OffsetDateTime nearestDate = nearestEntries.get(0).getValue();
            return nearestEntries.stream().filter(e -> e.getValue().equals(nearestDate)).map(Map.Entry::getKey).toList();
        }
    }

    private String getPlansString() {
        return this.plans.entrySet().stream().map(e ->
                        "id=%s,  pattern=%s,  date=%s".formatted(
                                e.getKey().getId(),
                                e.getKey().getRecurrencePattern(),
                                DATETIME_FORMATTER.format(e.getValue())))
                .collect(Collectors.joining(System.lineSeparator()));
    }

    private OffsetDateTime getNearestTaskDateTime(TreatmentPlan treatmentPlan) {
        return this.plans.get(treatmentPlan);
    }

    private void updateNextTaskDateTime(TreatmentPlan plan, OffsetDateTime nearestTaskStartTime) {
        this.plans.replace(plan, nearestTaskStartTime);
    }

    private void clearTaskDates() {
        this.plans.clear();
    }
}
